Source: cinnamon-desktop
Section: x11
Priority: optional
Maintainer: Debian Cinnamon Team <debian-cinnamon@lists.debian.org>
Uploaders:
 Maximiliano Curia <maxy@debian.org>,
 Margarita Manterola <marga@debian.org>,
 Fabio Fantoni <fantonifabio@tiscali.it>,
 Norbert Preining <norbert@preining.info>,
 Joshua Peisach <itzswirlz2020@outlook.com>,
 Christoph Martin <martin@uni-mainz.de>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-gir,
 gobject-introspection (>= 0.10.2-1~),
 gtk-doc-tools (>= 1.4),
 intltool (>= 0.40.6),
 libaccountsservice-dev,
 libgdk-pixbuf2.0-dev (>= 2.22.0),
 libgirepository1.0-dev (>= 0.10.2-1~),
 libglib2.0-dev (>= 2.37.3),
 libgtk-3-dev (>= 3.3.16),
 libpulse-dev (>= 12.99.3),
 libx11-dev,
 libxext-dev,
 libxkbfile-dev,
 libxml2-dev (>= 2.4.20),
 libxrandr-dev (>= 2:1.3),
 meson (>= 0.50.0),
 python3:any,
 xkb-data,
 yelp-tools,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://cinnamon.linuxmint.com/
Vcs-Browser: https://salsa.debian.org/cinnamon-team/cinnamon-desktop
Vcs-Git: https://salsa.debian.org/cinnamon-team/cinnamon-desktop.git

Package: cinnamon-desktop-data
Architecture: all
Breaks:
 cinnamon (<< 5.4),
 cinnamon-control-center (<< 5.4),
 cinnamon-screensaver (<< 5.4),
 cinnamon-session (<< 5.4),
 cinnamon-settings-daemon (<< 5.4),
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Common files for Cinnamon desktop apps
 This package includes files that are shared between several Cinnamon
 apps (i18n files and configuration schemas).

Package: gir1.2-cinnamondesktop-3.0
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: Introspection data for CinnamonDesktop
 This package contains the introspection data for CinnamonDesktop.
Breaks:
 cinnamon-common (<< 5.4),
 cinnamon-screensaver (<< 5.4),
 gir1.2-meta-muffin-0.0 (<< 5.4),

Package: gir1.2-cvc-1.0
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: Introspection data for Cinnamon pulseaudio abstraction
 This package contains the introspection data for Cinnamon pulseaudio
 abstraction.
Breaks: cinnamon (<< 5.4), cinnamon-settings-daemon (<< 5.4)

Package: libcinnamon-desktop-dev
Section: libdevel
Architecture: any
Depends:
 gir1.2-cinnamondesktop-3.0 (= ${binary:Version}),
 gir1.2-cvc-1.0 (= ${binary:Version}),
 libcinnamon-desktop4 (= ${binary:Version}),
 libcvc0 (= ${binary:Version}),
 libgtk-3-dev (>= 3.3.16),
 libpulse-dev,
 libxkbfile-dev,
 ${misc:Depends},
Description: Cinnamon library for loading .desktop files - development files
 This package provides the include files and static library for the Cinnamon
 desktop library functions.
Breaks: libmuffin-dev (<< 5.4)

Package: libcinnamon-desktop4
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 cinnamon-desktop-data (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: hwdata (>= 0.227-1)
Breaks:
 cinnamon-control-center (<< 5.4),
 cinnamon-settings-daemon (<< 5.4),
 libcinnamon-control-center1 (<< 5.4),
 nemo (<< 5.4),
Description: Cinnamon library for loading .desktop files
 This library is used by Cinnamon to load the .desktop files.

Package: libcvc0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Cinnamon pulseaudio abstraction library
 This library contains code to access PulseAudio using a GObject
 based library, shared between cinnamon-control-center, cinnamon-settings-daemon
 and cinnamon. It is not API stable, and it is meant to be used
 as a submodule.
Breaks: cinnamon-settings-daemon (<< 5.4)
